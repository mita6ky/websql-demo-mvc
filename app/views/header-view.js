//header view

(function(GLOBAL) {

    function HeaderView(options) {

        this.elements = {};
        var config = {
            name: 'header',
            model: options ? options.model : null
        };

        GLOBAL.BaseView.call(this, config);

        console.log('init header view');
    }

    App.extendView(HeaderView);

    HeaderView.prototype.bindEvents = function bindingEvents() {

        var view = this;
        view.elements.navItems = view.$placeholder.find(".nav-item");
        view.elements.navItems.on("click", view.handleNavChange.bind(view));
    };

    HeaderView.prototype.unbindEvents = function unbindingEvents() {

        var view = this;
        if(view.elements.navItems) {
            view.elements.navItems.off("click");
        }
    };

    HeaderView.prototype.handleNavChange = function handlingNavChange(evnt) {

        var route = $(evnt.target).attr('data-target');

        $(document).trigger('route:change', {newRoute: route});
    };

    GLOBAL.views = GLOBAL.views || {};
    GLOBAL.views.headerView = HeaderView;

})(window);