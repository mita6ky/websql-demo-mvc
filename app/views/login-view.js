//login view

(function(GLOBAL, $, App) {

    function LoginView(options) {

        var config = {
            name: 'login',
            model: options ? options.model : null
        };

        GLOBAL.BaseView.call(this, config);
    }

    App.extendView(LoginView);

    LoginView.prototype.bindEvents = function bindingEvents(){

        var view = this;
        this.$placeholder.find("#login-username").on("keyup", function(evnt){

            view.model.user.setName(this.value);
        });

        this.$placeholder.find("#login-userpassword").on("keyup", function(evnt){

            view.model.user.setPassword(this.value);
        });

    };

    LoginView.prototype.enterView = function enteringView() {

        this.$placeholder.show();
        $(document).off("model:" + this.modelName + ":update", this.smartUpdateMarkup);
    };

    LoginView.prototype.exitView = function exitingView() {

        $(document).on("model:" + this.modelName + ":update", this.smartUpdateMarkup.bind(this));
        this.$placeholder.hide();
    };

    LoginView.prototype.smartUpdateMarkup = function smartUpdatingMarkup() {

        this.$placeholder.find("#login-username").val(this.model.user.name);
        this.$placeholder.find("#login-userpassword").val(this.model.user.password);
    };

    GLOBAL.views = GLOBAL.views || {};
    GLOBAL.views.loginView = LoginView;

})(window, jQuery, App);