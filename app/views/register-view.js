//register view

(function(GLOBAL) {

    function RegisterView(options) {

        var config = {
            name: 'register',
            model: options ? options.model : null
        };

        GLOBAL.BaseView.call(this, config);

        console.log('init register view');
    }


    App.extendView(RegisterView);

    RegisterView.prototype.bindEvents = function bindingEvents(){

        var view = this;
        this.$placeholder.find("#register-username").on("keyup", function(evnt){

            view.model.user.setName(this.value);
        });

        this.$placeholder.find("#register-userpassword").on("keyup", function(evnt){

            view.model.user.setPassword(this.value);
        });

    };

    RegisterView.prototype.enterView = function enteringView() {

        console.log("enter register view");
        this.$placeholder.show();
        $(document).off("model:" + this.modelName + ":update", this.smartUpdateMarkup);

    };

    RegisterView.prototype.exitView = function exitingView() {

        console.log("exit register view");
        $(document).on("model:" + this.modelName + ":update", this.smartUpdateMarkup.bind(this));
        this.$placeholder.hide();
    };

    RegisterView.prototype.smartUpdateMarkup = function smartUpdatingMarkup() {

        this.$placeholder.find("#register-username").val(this.model.user.name);
        this.$placeholder.find("#register-userpassword").val(this.model.user.password);
    };

    GLOBAL.views = GLOBAL.views || {};
    GLOBAL.views.registerView = RegisterView;

})(window);